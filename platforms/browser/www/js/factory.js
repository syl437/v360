angular.module('starter.factories', [])

.factory('SendPostToServer', SendPostToServer)


function SendPostToServer($http,$rootScope,$ionicLoading) 
{
  return function(params,url,callback) 
  {
	  
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';	
		$http.post(url,params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				callback(data, true);
				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				callback(config, false);
			});						
  }
}














