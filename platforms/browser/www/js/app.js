// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngStorage','ngCordova','rzModule'])

.run(function($ionicPlatform,$rootScope,$localStorage) {
	
	$rootScope.phpHost = "http://v360.co.il/php/";
	$rootScope.generalSuppliers = [];
	$rootScope.diraArray = [];
	
	
	//$rootScope.favoritesArray = [];
	//$localStorage.favoritesArray = $rootScope.favoritesArray;
	
  $ionicPlatform.ready(function() {
	  
	window.ga.startTrackerWithId('UA-91420740-1');  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })

	
    .state('app.search', {
      url: '/search',
      views: {
        'menuContent': {
          templateUrl: 'templates/search.html',
          controller: 'SearchCtrl'
        }
      }
    })

	
	
    .state('app.results', {
      url: '/results',
      views: {
        'menuContent': {
          templateUrl: 'templates/results.html',
          controller: 'ResultsCtrl'
        }
      }
    })	

    .state('app.details', {
      url: '/details/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })	


    .state('app.suppliers', {
      url: '/suppliers',
      views: {
        'menuContent': {
          templateUrl: 'templates/suppliers.html',
          controller: 'SuppliersCtrl'
        }
      }
    })	

    .state('app.supplierinfo', {
      url: '/supplierinfo/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/supplierinfo.html',
          controller: 'SupplierInfoCtrl'
        }
      }
    })		


    .state('app.agent', {
      url: '/agent/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/agent.html',
          controller: 'AgentCtrl'
        }
      }
    })	

	
    .state('app.supplier', {
      url: '/supplier/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/supplier.html',
          controller: 'SupplierCtrl'
        }
      }
    })	

    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    })	

    .state('app.contact', {
      url: '/contact',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })

    .state('app.favorites', {
      url: '/favorites',
      views: {
        'menuContent': {
          templateUrl: 'templates/favorites.html',
          controller: 'FavoritesCtrl'
        }
      }
    })

    .state('app.contactsupplier', {
      url: '/contactsupplier/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })		
	
	;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
