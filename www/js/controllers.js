angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
	
  
	$scope.areas = [];
	$scope.areas[1] = "צפון";
	$scope.areas[2] = "חיפה והסביבה";
	$scope.areas[3] = "קריות והסביבה";
	$scope.areas[4] = "עכו - נהריה והסביבה";
	$scope.areas[5] = "גליל עליון";
	$scope.areas[6] = "הכנרת והסביבה";
	$scope.areas[7] = "כרמיאל והסביבה";
	$scope.areas[8] = "נצרת - שפרעם והסביבה";
	$scope.areas[9] = "ראש פינה החולה";
	$scope.areas[10] = "גליל תחתון";
	$scope.areas[11] = "הגולן";
	$scope.areas[12] = "חדרה זכרון ועמקים";
	$scope.areas[13] = "זכרון וחוף הכרמל";
	$scope.areas[14] = "חדרה והסביבה";
	$scope.areas[15] = "קיסריה והסביבה";
	$scope.areas[16] = "יקנעם טבעון והסביבה";
	$scope.areas[17] = "עמק בית שאן";
	$scope.areas[18] = "עפולה והעמקים";
	$scope.areas[19] = "רמת מנשה";
	$scope.areas[20] = "השרון";
	$scope.areas[21] = "נתניה והסביבה";
	$scope.areas[22] = "רמת השרון - הרצליה";
	$scope.areas[23] = "רעננה - כפר סבא";
	$scope.areas[24] = "הוד השרון והסביבה";
	$scope.areas[25] = "דרום השרון";
	$scope.areas[26] = "צפון השרון";
	$scope.areas[27] = "מרכז";
	$scope.areas[28] = "תל אביב";
	$scope.areas[29] = "תל אביב - מרכז";
	$scope.areas[30] = "תל אביב - צפון";
	$scope.areas[31] = "תל אביב - דרום";
	$scope.areas[32] = "תל אביב -  מזרח";
	$scope.areas[33] = "ראשון לציון והסביבה";
	$scope.areas[34] = "חולון - בת ים";
	$scope.areas[35] = "רמת גן - גבעתיים";
	$scope.areas[36] = "פתח תקווה והסביבה";
	$scope.areas[37] = "ראש העין והסביבה";
	$scope.areas[38] = "בקעת אונו";
	$scope.areas[39] = "רמלה - לוד";
	$scope.areas[40] = "בני ברק - גבעת שמואל";
	$scope.areas[41] = "עמק  איילון";
	$scope.areas[42] = "שוהם והסביבה";
	$scope.areas[43] = "מודיעין והסביבה";
	$scope.areas[44] = "אזור ירושלים";
	$scope.areas[45] = "ירושלים",
	$scope.areas[46] = "בית שמש והסביבה";
	$scope.areas[47] = "הרי יהודה - מבשרת והסביבה";
	$scope.areas[48] = "מעלה אדומים והסביבה";
	$scope.areas[49] = "יהודה שומרון ובקעת הירדן";
	$scope.areas[50] = "ישובי דרום ההר";
	$scope.areas[51] = "ישובי שומרון";
	$scope.areas[52] = "גוש עציון";
	$scope.areas[53] = "בקעת הירדן וצפון ים המלח";
	$scope.areas[54] = "אריאל וישובי יהודה";
	$scope.areas[55] = "שפלה מישור חוף דרומי";
	$scope.areas[56] = "נס ציונה - רחובות";
	$scope.areas[57] = "אשדוד - אשקלון והסביבה";
	$scope.areas[58] = "גדרה - יבנה והסביבה";
	$scope.areas[59] = "קרית גת והסביבה";
	$scope.areas[60] = "שפלה";
	$scope.areas[61] = "דרום";
	$scope.areas[62] = "באר שבע והסביבה";
	$scope.areas[63] = "אילת וערבה";
	$scope.areas[64] = "ישובי הנגב";
	$scope.areas[65] = "הנגב המערבי";
	$scope.areas[66] = "דרום ים המלח";
	$scope.areas[67] = "חו\"ל";
	
  
  //$scope.getAreaName(0);
  
  $scope.getAreaName = function(id)
  {
	  return ($scope.areas[id]);
  }


  
	$scope.dialSupplier = function(value)
	{
		if (value)
			window.location ="tel://"+value;
	}
	
	$scope.mailSupplier = function(value)
	{
		if (value)
			window.location ="mailto:"+value;
	}		

	
})




.controller('MainCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,SendPostToServer,$sce) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.mainiframeURL = '';

	$scope.fields = 
	{
		"search" : ""
	}
	
	$scope.getSuppliers = function()
	{
		params = {};
		
		
				
		SendPostToServer(params,$rootScope.phpHost+"/get_all_suppliers.php",function(data, success) 
		{
			$scope.generalSuppliers = data;
			$rootScope.generalSuppliers = $scope.generalSuppliers;
						
		});	
	
	}
	
	$scope.getSuppliers();

	
	$scope.getMain = function()
	{
	
		params = { };
		$scope.countMatches = 0;
		
				
		SendPostToServer(params,$rootScope.phpHost+"/get_main.php",function(data, success) 
		{
			$ionicLoading.hide();
			$scope.searchArray = data;
			$rootScope.diraArray = data;
			
			for(i=0;i< $scope.searchArray.length;i++)
			{
					if ($scope.searchArray[i].mainpage == 1)
						$scope.matterportId = $scope.searchArray[i].matterport;		
			}	

			
			$scope.matterportURL = 'https://my.matterport.com/show/?m='+$scope.matterportId+'&play=1&qs=1';
			$scope.mainiframeURL = $sce.trustAsResourceUrl($scope.matterportURL);

		});	
	
	}
	
	
	$scope.getMain();
	
	
	$scope.doSearch = function()
	{
		if ($scope.fields.search =="")
		{
		   $ionicPopup.alert({
			 title: 'יש להזין שם פרויקט לחיפוש',
			 template: ''
		   });				
		}
		else
		{
			params = 
			{
				"search" : $scope.fields.search,
			};
			
					
			SendPostToServer(params,$rootScope.phpHost+"/main_search.php",function(data, success) 
			{
					$ionicLoading.hide();
					$scope.fields.search  = '';
					$rootScope.diraArray = data;
					console.log("results:" , data.length);
					console.log(data)
					
					
					if (data.length > 0)
					{
						window.location = "#/app/results";
					}
					else
					{
						   $ionicPopup.alert({
							 title: 'לא נמצאו תוצאות יש לנסות שוב',
							 template: ''
						   });				
					}
					
			});	
		}
	}

	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.doSearch();
		}
	}


})


.controller('SuppliersCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,SendPostToServer) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.suppliersList = $rootScope.generalSuppliers;
	


})


.controller('SupplierInfoCtrl', function($scope,$ionicLoading,$stateParams,$http,$rootScope,$ionicPopup,SendPostToServer) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.itemId = $stateParams.ItemId;
	

	$scope.getSupplier = function()
	{
		params = 
		{
			"id" : $scope.itemId,
		};
		
				
		SendPostToServer(params,$rootScope.phpHost+"/get_supplier.php",function(data, success) 
		{
			console.log("supplierInfo",data);
			$scope.supplierInfo = data[0];

		});			
	}
	
	$scope.getSupplier();
	
	
	

	


})

.controller('AgentCtrl', function($scope,$ionicLoading,$stateParams,$http,$rootScope,$ionicPopup,SendPostToServer) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.itemId = $stateParams.ItemId;
	
	$scope.getAgent = function()
	{
		params = 
		{
			"id" : $scope.itemId,
		};
		
				
		SendPostToServer(params,$rootScope.phpHost+"/get_agent.php",function(data, success) 
		{
			console.log("agentInfo",data);
			$scope.agentInfo = data[0];

		});			
	}
	
	$scope.getAgent();
	


	

})

.controller('SearchCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,SendPostToServer) {


	$scope.search = 
	{
		"area" : "",
		"sugdira" : "",
		"price" : "" , //0,
		"rooms" : "",
		"floor" : 0,
		"ceil" : "", //6000000,
		"high" : "", //6000000
	}

		

	$scope.searchBtn = function()
	{

		params = 
		{
			"area" : $scope.search.area,
			"sugdira" : $scope.search.sugdira,
			"price" : $scope.search.price,
			"maxprice" : $scope.search.high,
			"rooms" : $scope.search.rooms,
		};
		
		//alert ($scope.search.price)
		//alert ($scope.search.high)
		//return;
				
		SendPostToServer(params,$rootScope.phpHost+"/search_dira.php",function(data, success) 
		{
			$ionicLoading.hide();
			
			$rootScope.diraArray = data;
			
			
			console.log("results:" , data.length);
			console.log(data)
			
			
			if (data.length > 0)
			{
				window.location = "#/app/results";
			}
			else
			{
				   $ionicPopup.alert({
					 title: 'לא נמצאו תוצאות יש לנסות שוב',
					 template: ''
				   });				
			}
		});	

					
	}


})


.controller('ResultsCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.searchArray = $rootScope.diraArray;
	$scope.TypeSelected = 1;
	
	$scope.changeType = function(type)
	{
		$scope.TypeSelected = type;
	}
	
})


.controller('DetailsCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,$stateParams,$sce,$ionicModal,SendPostToServer,$localStorage,ClosePopupService) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.searchArray = $rootScope.diraArray;
	$scope.itemId = $stateParams.ItemId;
	$scope.foundInArray = 0;
	$scope.diraData = "";
	$scope.supplierInfo = "";
	
	if (!$localStorage.favoritesArray)
		$localStorage.favoritesArray = [];
	
	$scope.favoritesArray = $localStorage.favoritesArray; //$rootScope.favoritesArray;
	$scope.existsFavorites = 0;
		

		
	$scope.getSingle = function()
	{
		params = 
		{
			"id" : $scope.itemId,
		};
		
				
		SendPostToServer(params,$rootScope.phpHost+"/get_single.php",function(data, success) 
		{
			$scope.diraData = data[0];
			$scope.supplierInfo =  $scope.diraData.supplier;
			$scope.matterportId = 'https://my.matterport.com/show/?m='+$scope.diraData.matterport+'&play=1&qs=1';
			$scope.iframe_url = $sce.trustAsResourceUrl($scope.matterportId);
			console.log ("info: " , $scope.diraData);
		});					
	}

			
	$scope.checkinArray = function()
	{
		for(i=0;i< $scope.searchArray.length;i++)
		{
			if ($scope.searchArray[i].index == $scope.itemId)
			{
				$scope.foundInArray = 1;
				$scope.diraData = $scope.searchArray[i];
				$scope.supplierInfo =  $scope.searchArray[i].supplier;

			}
		}		
		
		if ($scope.foundInArray == 0)
		{
			$scope.getSingle();
		}
	}	


	$scope.getSingle();
	
	

	//alert ($scope.diraData.map)
	
	$scope.AddFavorite = function()
	{
		if ($scope.favoritesArray.length > 0)
		{
			for(i=0;i< $scope.favoritesArray.length;i++)
			{
				if ($scope.favoritesArray[i].index != $scope.itemId)
					$scope.favoritesArray.push($scope.diraData);
			}			
		}
		else
		{
			$scope.favoritesArray.push($scope.diraData);
		}
		


		   $ionicPopup.alert({
			 title: 'נוסף למועדפים בהצלחה',
			 template: ''
		   });	
		
		
		console.log($scope.favoritesArray)
	}	
	
	
	$scope.showMap = function()
	{
		if ($scope.diraData.map)
		{
			
			
		  $scope.map_url = $sce.trustAsResourceUrl($scope.diraData.map);


			
		  $ionicModal.fromTemplateUrl('templates/map.html', {
			scope: $scope
		  }).then(function(registerModal) {
			$scope.registerModal = registerModal;
			$scope.registerModal.show();
		  });				
		}
		else
		{
		   $ionicPopup.alert({
			 title: 'לא הוגדרה מפה נסה שוב מאוחר יותר',
			 template: ''
		   });			
		}
	}
	
	$scope.closeMapModal = function()
	{
		$scope.registerModal.hide();
	}
	
	$scope.SharePage = function()
	{
		$scope.ShareURL = 'http://v360.co.il/דירות/'+$scope.diraData.index+'/'+$scope.diraData.title+'';
		
		iabRef = window.open("https://www.facebook.com/sharer/sharer.php?u="+$scope.ShareURL, '_blank', 'location=yes');
		
		
		//window.plugins.socialsharing.share('V360', null, null, $scope.ShareURL);
		
		
	}
	
	


	
	
	$scope.numberWithCommas = function(x) 
	{
		if(x)
   		 return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	

	
	$scope.showImgPopUp = function (x) 
	{
		$scope.currentImage = x;
		myPopup = $ionicPopup.show({
			templateUrl: 'templates/img_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});

		ClosePopupService.register(myPopup);
			
	}	
	
	
	$scope.hideInfo = function ()
	{
         myPopup.close();
    };

	
})



.controller('FavoritesCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,$stateParams,SendPostToServer,$localStorage) {
	
	$scope.serverHost = $rootScope.phpHost;
	
	if (!$localStorage.favoritesArray)
		$localStorage.favoritesArray = [];
	
	$scope.FavoritesArray = $localStorage.favoritesArray; //$rootScope.favoritesArray;
	$scope.Favorites = $scope.FavoritesArray.reverse();
	
	$scope.deleteFav = function(index)
	{
		$scope.FavoritesArray.splice(index, 1);
	}

})


.controller('SupplierCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,$stateParams,SendPostToServer) {

	$scope.serverHost = $rootScope.phpHost;
	$scope.itemId = $stateParams.ItemId;
	
	$scope.getSupplier  = function()
	{
		params = 
		{
			"id" : $scope.itemId,
		};
		
				
		SendPostToServer(params,$rootScope.phpHost+"/get_supplier.php",function(data, success) 
		{
			$scope.supplierInfo = data[0];
			$scope.supplierList = $scope.supplierInfo.deals;
			console.log("supplier:" , data);

		});			
	}
	
	$scope.getSupplier();
	

	
})


.controller('AboutCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,$stateParams) {


})


.controller('ContactCtrl', function($scope,$ionicLoading,$http,$rootScope,$ionicPopup,$stateParams,SendPostToServer) {

	$scope.agentId = $stateParams.ItemId;
	
	if ($scope.supplierId == "")
		$scope.contactType = "contact";
	else
		$scope.contactType = "agent";


	$scope.contact = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"desc" : "",
	}
	
	$scope.SendContact = function()
	{
		if ($scope.contact.name =="")
		{
		   $ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			 template: ''
		   });				
		}
		
		else if ($scope.contact.phone =="")
		{
		   $ionicPopup.alert({
			 title: 'יש להזין טלפון',
			 template: ''
		   });				
		}
		else if ($scope.contact.mail =="")
		{
		   $ionicPopup.alert({
			 title: 'יש להזין כתובת מייל',
			 template: ''
		   });				
		}		
		else if ($scope.contact.desc =="")
		{
		   $ionicPopup.alert({
			 title: 'יש להזין פרטי הפנייה',
			 template: ''
		   });				
		}	
		else
		{
			
			
		params = 
		{
			"name" : $scope.contact.name,
			"phone" : $scope.contact.phone,
			"mail" : $scope.contact.mail,
			"desc" : $scope.contact.desc,
			"type" : $scope.contactType,
			"agentid": $scope.agentId,		
			"send" : 1,
		};
		
				
		SendPostToServer(params,$rootScope.phpHost+"/contact.php",function(data, success) 
		{

			   $ionicPopup.alert({
				 title: 'פרטיך התקבלו בהצלחה, נחזור אליך בהקדם האפשרי',
				 template: ''
			   });	
			   
			   $scope.contact.name = '';
			   $scope.contact.phone = '';
			   $scope.contact.mail = '';
			   $scope.contact.desc = '';
		});	

		}
	}



})


.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
        var lastPopup;
        return {
            register: function(popup) {
                $timeout(function(){
                    var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                    if(!element || !popup || !popup.close) return;
                    element = element && element.children ? angular.element(element.children()[0]) : null;
                    lastPopup  = popup;
                    var insideClickHandler = function(event){
                        event.stopPropagation();
                    };
                    var outsideHandler = function() {
                        popup.close();
                    };
                    element.on('click', insideClickHandler);
                    $document.on('click', outsideHandler);
                    popup.then(function(){
                        lastPopup = null;
                        element.off('click', insideClickHandler);
                        $document.off('click', outsideHandler);
                    });
                });
            },
            closeActivePopup: function(){
                if(lastPopup) {
                    $timeout(lastPopup.close);
                    return lastPopup;
                }
            }
        };
})
	
	